const router = require ('express' ).Router()
const passport = require('./lib/passport')
const bodyParser = require('body-parser')
const restrictApi = require('./middlewares/restrict-api')


// Controllers
const auth = require ('./controllers/authController' )

const restrict = require('./middlewares/restrict')

// Homepage
router.get('/', restrict, (req, res) => res.render('index'))

// Register Page
router .get('/register' , (req, res) => res.render ('register' ))
router .post('/register' , auth.register )

router.use(bodyParser.json())

/// Login Page
router.get('/login', (req, res) => res.render('login'))
router.post('/login', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
}))

router.get('/whoami', restrict, auth.whoami)

// Import passport JWT
router.post('/api/v1/auth/login', auth.login)
router.get('/api/v1/auth/whoami', restrictApi, auth.whoamiApi)

module.exports = router;
